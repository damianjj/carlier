// carlier.cpp: definiuje punkt wejścia dla aplikacji konsolowej.
//

#include "stdafx.h"
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>


using namespace std;

struct Process
{
	int r; // termin dostępnosci
	int p; // czas obsługi
	int q; // czas dostarczenia zadania
	int s; // czas rozpoczecia wykonywania na maszynie
	int n; // indeks procesu

};
vector <Process> procesy;   // tablica zadan
vector <int> pi;    // permutacja wykonywania zadan na maszynie

struct CompareLess // struktura pomocnicza do sortowania po r
{
	CompareLess() {};
	~CompareLess() {};

	bool operator()(const Process &p1, const Process &p2)
	{
		return (p1.r > p2.r);
	}
};

struct CompareGreater // struktura pomocnicza do sortowania po q
{
	CompareGreater() {};
	~CompareGreater() {};

	bool operator()(const Process &p1, const Process &p2)
	{
		return (p1.q < p2.q);
	}
};

priority_queue<Process, vector<Process>, CompareLess> N; // nieuszeregowane zadania
priority_queue<Process, vector<Process>, CompareGreater> G; // gotowe zadania do realizacji


bool loadFromFile(int &amount) // załadowanie pliku do struktury
{
	Process process;
	ifstream file;
	string fileName;

	cout << "Podaj nazwe pliku" << endl;
	cin >> fileName;
	fileName += ".DAT";

	file.open(fileName.c_str());

	if (file.good())
		cout << "Udalo sie otworzyc plik" << endl;
	else
	{
		cout << "Nie udalo sie otworzyc pliku" << endl;
		return false;
	}

	file >> amount;

	int n = 0;
	while (!file.eof())
	{
		file >> process.r >> process.p >> process.q;
		process.n = n;
		procesy.push_back(process);
		n++;
	}
}

template<typename T> void printQueue(T& queue)
{
	while (!queue.empty())
	{
		cout << queue.top().r << " " << queue.top().p << " " << queue.top().q;
		cout << endl;
		queue.pop();
	}
}
void wypelnij_tablice()
{

	for (int i = 0; i<procesy.size(); i++) {
		pi.push_back(0);

	}

}
int Schrage()
{
	int t = 0;
	int Cmax = 0;
	int k = 0;

	for (int i = 0; i<procesy.size(); i++)
		N.push(procesy[i]);


	while (!G.empty() || !N.empty()) {
		while (!N.empty() && N.top().r <= t) {   // jezeli N nie jest puste i N.r mieści się w czasie t

			G.push(N.top());
			N.pop();

		}
		if (G.empty()) {
			t = N.top().r;  //zwiększ czas do czasu dostępności kolejnego zadania

		}
		else
		{
			pi[k] = G.top().n;               //wpisywanie do tablicy permutacji wykonywania zadan po indeksach 
			procesy[pi[k]].s = t;           //zapamietanie czasu rozpoczecia wykonywania zadania na maszynie
			k++;
			t = t + G.top().p;


			Cmax = max(Cmax, t + G.top().q);  //wyznaczanie najpóźniejszego momentu dostarczenia
			G.pop();
		}


	}
	return Cmax;
}

int Schrage_2()
{
	int t = 0;
	int Cmax = 0;

	Process l;
	l.r = 0;
	l.p = 0;
	l.q = 9999999999999999999; // podstawienie dużej wartosci, aby zabezpieczyc przed przerwaniem zadania 0 w kroku 5

	for (int i = 0; i<procesy.size(); i++)
		N.push(procesy[i]);

	while (!G.empty() || !N.empty()) {
		while (!N.empty() && N.top().r <= t) {
			Process e = N.top();              // aktualizacja zadań gotowych
			G.push(N.top());
			N.pop();
			if (e.q>l.q)           // sprawdzenie, czy zadanie wlasnie dostepne ma wiekszy priorytet
			{
				l.p = t - e.r;         // obliczanie pozostałej wartosci zadania przerwanego
				t = e.r;                // cofniecie chwili czasowej, aby do kolejki trafily tylko zadania z momentem dostepnosci
										// mniejszym badz rownym momentowi przerwania 
										// moment rozpoczecia dla zadania ktore przerywa
										//umozliwia wybranie z zadan o najwiekszym q zadanie o najwiekszym r  
				if (l.p>0) G.push(l); // dodawanie zadania przerwanego ponownie do zadan gotowych
			}

		}
		if (G.empty()) {
			t = N.top().r;     // przesuniecie chwili czasowej do momentu dostepnosci kolejnego zadania


		}
		else {

			Process l1;         //uaktualnianie zmiennej l
			l1 = G.top();
			G.pop();
			l = l1;
			t = t + l1.p;    // obliczanie momentu zdjecia z maszyny 
			Cmax = max(Cmax, t + l1.q);   // wyznaczanie momentu dostarczenia do klienta
		}
	}

	return Cmax;
}

void sciezka_krytyczna(int &a, int &b, int &c, int &C, int UB)
{

	int tmp = 0;

	for (int i = 0; i<procesy.size(); i++)         //wyznaczanie b - zadania konczacego caly proces, koniec sciezki krytycznej
	{

		if (procesy[pi[i]].s + procesy[pi[i]].p + procesy[pi[i]].q>tmp)
		{
			b = i;
			tmp = procesy[pi[i]].s + procesy[pi[i]].p + procesy[pi[i]].q;
		}
	}

	for (int j = 0; j <procesy.size(); j++)      //wyznaczanie a - zadanie rozpoczynajace sciezke krytyczna
	{
		int suma = 0;
		for (int s = j; s <= b; s++)
		{
			suma += procesy[pi[s]].p;
		}

		if (UB == procesy[pi[j]].r + suma + procesy[pi[b]].q)
		{
			a = j;
			break;
		}
	}

	for (int j = a; j <= b; j++)
	{
		if (procesy[pi[j]].q < procesy[pi[b]].q)     // wyznaczanie zadania interferencyjnego
		{                                           //  zadanie w sciezke krytycznej, ktorego czas dostarczenia
			c = j;                                  // jest mniejszy od czasu dostarczenia zadania b
			C = 2;
		}
	}

}

int znajdz_qmin(int &qmin, int c, int b)
{
	for (int i = c + 1; i<b; i++)
	{
		if (procesy[pi[i]].q<qmin)
			qmin = procesy[pi[i]].q;
	}
	return qmin;
}
int znajdz_rmin(int &rmin, int c, int b)
{
	for (int i = c + 1; i<b; i++)
	{
		if (procesy[pi[i]].r<rmin)
			rmin = procesy[pi[i]].r;
	}
	return rmin;
}

int suma_prim(int c, int b)
{
	int suma = 0;
	for (int i = c + 1; i<b; i++)
	{
		suma += procesy[pi[i]].p;
	}
	return suma;
}


int Carlier(int UB)
{
	int t = 0;
	int a = 0, b = 0, c = 0;
	int LB;
	int C = -1; //zmienia wartosc jesli zadanie referencyjne istnieje


	wypelnij_tablice();



	int Cmax = Schrage();

	// aktualizacja najlepszego do tej pory rozwiazania
	if (Cmax<UB)
	{
		UB = Cmax;
	}



	//wyznaczanie sciezki krytycznej i zadania interferencyjnego


	sciezka_krytyczna(a, b, c, C, Cmax);
	// jesli nie znaleziona zadania interferencyjnego, Schrage wygenerowal rozwiazanie optymalne
	if (C == -1)
	{
		return UB;

	}

	//
	int qmin = procesy[pi[c + 1]].q;
	int rmin = procesy[pi[c + 1]].r;
	int suma = 0;
	int rc = procesy[pi[c]].r;           //zapamietanie wartosci do odtworzenia
	int qc = procesy[pi[c]].q;
	znajdz_qmin(qmin, c, b);            //wyznaczenie najmniejzego z terminow dostepnosci
										//i terminow dostarczenia sposrod zadan c+1 do b
	znajdz_rmin(rmin, c, b);
	suma = suma_prim(c, b);            //wyznaczenie sumy czasow wykonywania zadan od c+1 do b

	procesy[pi[c]].r = max(procesy[pi[c]].r, rmin + suma);  //modyfikacja terminu dostepnosci zadania referencyjnego
															// wymuszanie aby zadanie referencyjne bylo wykonywane 
	LB = Schrage_2();                                       // zadaniami c+1 do b

	if (LB<UB)                                         //wyznaczenie dolnej granicy
	{
		Cmax = Carlier(UB);
		return Cmax;
	}

	procesy[pi[c]].r = rc;                         //odtworzenie wartosci dostepnosci zadania referencyjnego

												   //modyfikacja terminu dostarczenia zadania referencyjnego
												   // wymuszanie aby zadanie bylo wykonane przed zadaniami c+1 do b    
	procesy[pi[c]].q = max(procesy[pi[c]].q, qmin + suma);

	LB = Schrage_2();                     //wyznaczenie dolnej granicy

	if (LB<UB)
	{
		Cmax = Carlier(UB);
		return Cmax;
	}

	procesy[pi[c]].q = qc;               //odtworzenie dostarczenia zadania referencyjnego


	return Cmax;



}





int main()
{
	int amount = 0;
	loadFromFile(amount);
	int UB = 10000000;     //zadanie duzej wartosci, aby nie przerwalo w poczatkowej fazie wykonywania algorytmu

	cout << "Cmax: " << Carlier(UB) << endl;

	system("pause");
	return 0;
}
